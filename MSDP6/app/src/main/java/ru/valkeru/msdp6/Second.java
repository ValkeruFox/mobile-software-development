package ru.valkeru.msdp6;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

public class Second extends AppCompatActivity {
    private ArrayAdapter<String> list1Adapter;
    private ArrayAdapter<String> list2Adapter;
    private ListView list1;
    private ListView list2;
    private EditText input;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.second);
        super.onCreate(savedInstanceState);

        input = findViewById(R.id.input);
        list1 = findViewById(R.id.list1);
        list2 = findViewById(R.id.list2);

        list1Adapter = new ArrayAdapter<>(this, R.layout.list_element);
        list2Adapter = new ArrayAdapter<>(this, R.layout.list_element);

        list1.setAdapter(list1Adapter);
        list2.setAdapter(list2Adapter);

        String[] list1Values = getResources().getStringArray(R.array.list1);
        String[] list2Values = getResources().getStringArray(R.array.list2);
        list1Adapter.addAll(list1Values);
        list2Adapter.addAll(list2Values);
    }

    public void addToList1(View view) {
        String text = input.getText().toString();
        if (text.equals("")) {
            return;
        }
        list1Adapter.add(text);
        input.setText("");
    }

    public void addToList2(View view) {
        String text = input.getText().toString();
        if (text.equals("")) {
            return;
        }
        list2Adapter.add(text);
        input.setText("");
    }

    public void onBackButtonPressed(View view) {
        onBackPressed();
    }
}
