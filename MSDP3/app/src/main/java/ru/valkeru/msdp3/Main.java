package ru.valkeru.msdp3;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Main extends AppCompatActivity {
    private EditText input1;
    private EditText input2;
    private TextView resultField;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.main);
        super.onCreate(savedInstanceState);

        input1 = findViewById(R.id.first);
        input2 = findViewById(R.id.second);
        resultField = findViewById(R.id.result_field);
    }

    public void add(View view) {
        String input1 = this.input1.getText().toString();
        String input2 = this.input2.getText().toString();

        if (input1.equals("") || input2.equals("")) {
            return;
        }

        int first = Integer.parseInt(input1);
        int second = Integer.parseInt(input2);

        resultField.setText(String.valueOf(first + second));

    }

    public void subtract(View view) {
        String input1 = this.input1.getText().toString();
        String input2 = this.input2.getText().toString();

        if (input1.equals("") || input2.equals("")) {
            return;
        }

        int first = Integer.parseInt(input1);
        int second = Integer.parseInt(input2);

        resultField.setText(String.valueOf(first - second));
    }

    public void multiply(View view) {
        String input1 = this.input1.getText().toString();
        String input2 = this.input2.getText().toString();

        if (input1.equals("") || input2.equals("")) {
            return;
        }

        int first = Integer.parseInt(input1);
        int second = Integer.parseInt(input2);

        resultField.setText(String.valueOf(first * second));
    }

    public void divide(View view) {
        String input1 = this.input1.getText().toString();
        String input2 = this.input2.getText().toString();

        if (input1.equals("") || input2.equals("")) {
            return;
        }

        int first = Integer.parseInt(input1);
        int second = Integer.parseInt(input2);

        resultField.setText(String.valueOf(first / second));
    }
}
