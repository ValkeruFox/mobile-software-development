package ru.valkeru.msdp4;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class Main extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ArrayAdapter<String> listAdapter;
    private ListView listView;
    private EditText input;
    private int currentPosition;
    private View currentElement;
    private Button editButton;
    private Button deleteButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.main);
        super.onCreate(savedInstanceState);

        listAdapter = new ArrayAdapter<>(this, R.layout.list_element);
        listView = findViewById(R.id.list);
        input = findViewById(R.id.input);
        editButton = findViewById(R.id.btnEdit);
        deleteButton = findViewById(R.id.btnDel);
        editButton.setEnabled(false);
        deleteButton.setEnabled(false);

        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (currentElement != null) {
            currentElement.setBackgroundColor(getResources().getColor(R.color.white, null));
        }

        currentElement = view;

        view.setBackgroundColor(getResources().getColor(R.color.colorAccent, null));
        String text = listAdapter.getItem(position);
        currentPosition = position;
        input.setText(text);
        editButton.setEnabled(true);
        deleteButton.setEnabled(true);
    }

    public void addListElement(View view) {
        String text = input.getText().toString();
        if (text.equals("")) {
            return;
        }

        listAdapter.add(text);
        input.setText("");
    }

    public void clearList(View view) {
        listAdapter.clear();
        input.setText("");
        editButton.setEnabled(false);
        deleteButton.setEnabled(false);
    }

    public void editElement(View view) {
        String actualText = listAdapter.getItem(currentPosition);
        listAdapter.remove(actualText);
        listAdapter.insert(input.getText().toString(), currentPosition);
    }

    public void deleteElement(View view) {
        currentElement.setBackgroundColor(getColor(R.color.white));
        currentElement = null;
        listAdapter.remove(listAdapter.getItem(currentPosition));
        input.setText("");
        editButton.setEnabled(false);
        deleteButton.setEnabled(false);
    }
}
