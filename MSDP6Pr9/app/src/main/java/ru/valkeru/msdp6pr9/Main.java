package ru.valkeru.msdp6pr9;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

public class Main extends AppCompatActivity {
    private SharedPreferences sharedPreferences;
    private EditText numericInput;
    private EditText textInput1;
    private EditText textInput2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.main);
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);
        numericInput = findViewById(R.id.numeric_input);
        textInput1 = findViewById(R.id.text_input_1);
        textInput2 = findViewById(R.id.text_input_2);
    }

    @Override
    protected void onResume() {
        super.onResume();

        int firstPreference = sharedPreferences.getInt("first", 1);
        String secondPreference = sharedPreferences.getString("second", "default");
        String thirdPreference = sharedPreferences.getString("third", "other default");

        numericInput.setText(String.valueOf(firstPreference));
        textInput1.setText(secondPreference);
        textInput2.setText(thirdPreference);
    }

    @Override
    protected void onPause() {
        super.onPause();

        int firstPreference = Integer.parseInt(numericInput.getText().toString());
        String secondPreference = textInput1.getText().toString();
        String thirdPreference = textInput2.getText().toString();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("first", firstPreference);
        editor.putString("second", secondPreference);
        editor.putString("third", thirdPreference);
        editor.apply();
    }
}
