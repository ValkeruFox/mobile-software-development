package ru.valkeru.msdp5pr7;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Main extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.main);
        super.onCreate(savedInstanceState);
    }

    public void startSecondActivity(View view) {
        Intent intent = new Intent(this, Second.class);
        startActivity(intent);
    }
}
