package ru.valkeru.msdp7;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ListView;

import ru.valkeru.msdp7.adapter.ColorAdapter;

public class Main extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.main);
        super.onCreate(savedInstanceState);

        ListView listView = findViewById(R.id.main_list);
        String[] listArray = getResources().getStringArray(R.array.animals);
        ColorAdapter colorAdapter = new ColorAdapter(listArray);
        listView.setAdapter(colorAdapter);
    }
}
