package ru.valkeru.msdp7.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import ru.valkeru.msdp7.R;

public class ColorAdapter extends BaseAdapter {
    private String[] names;

    public ColorAdapter(String[] names) {
        this.names = names;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return names[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent, false);
        }
        TextView textView = (TextView) convertView;
        textView.setText(names[position]);
        if (position % 2 == 0) {
            textView.setBackgroundColor(context.getResources().getColor(R.color.evenElementColor, null));
        } else {
            textView.setBackgroundColor(context.getResources().getColor(R.color.oddElementColor, null));
        }
        return convertView;
    }
}
