package ru.valkeru.msdp8;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import ru.valkeru.msdp8.adapter.ColorAdapter;

public class Second extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ListView listView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.second);
        super.onCreate(savedInstanceState);

        listView = findViewById(R.id.color_picker);
        String[] names = getResources().getStringArray(R.array.color_names);
        int[] values = getResources().getIntArray(R.array.color_values);
        ColorAdapter colorAdapter = new ColorAdapter(names, values);
        listView.setAdapter(colorAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE);
        if (sharedPreferences.contains(getString(R.string.background_color_parameter))) {
            int backgroundColor = (int) sharedPreferences.getLong(getString(R.string.background_color_parameter), R.color.colorPrimary);
            getWindow().getDecorView().setBackgroundColor(backgroundColor);
        }
    }

    public void handleBackButton(View view) {
        onBackPressed();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(getString(R.string.background_color_parameter), listView.getAdapter().getItemId(position));
        editor.apply();
        getWindow().getDecorView().setBackgroundColor((int) listView.getAdapter().getItemId(position));
        onBackPressed();
    }
}
