package ru.valkeru.msdp8;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Main extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.main);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE);
        if (sharedPreferences.contains(getString(R.string.background_color_parameter))) {
            int backgroundColor = (int) sharedPreferences.getLong(getString(R.string.background_color_parameter), R.color.colorPrimary);
            getWindow().getDecorView().setBackgroundColor(backgroundColor);
        }
    }

    public void callSecondActivity(View view) {
        Intent intent = new Intent(this, Second.class);
        startActivity(intent);
    }
}
