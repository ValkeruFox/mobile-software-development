package ru.valkeru.msdp8.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import ru.valkeru.msdp8.R;

public class ColorAdapter extends BaseAdapter {
    private String[] colorNames;
    private int[] colorCodes;

    public ColorAdapter(String[] colorNames, int[] colorCodes) {
        this.colorNames = colorNames;
        this.colorCodes = colorCodes;
    }

    @Override
    public int getCount() {
        return colorNames.length;
    }

    @Override
    public Object getItem(int position) {
        return colorNames[position];
    }

    @Override
    public long getItemId(int position) {
        return colorCodes[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent, false);
        }
        TextView textView = (TextView) convertView;
        textView.setText(colorNames[position]);
        textView.setBackgroundColor(colorCodes[position]);
        return convertView;
    }
}
