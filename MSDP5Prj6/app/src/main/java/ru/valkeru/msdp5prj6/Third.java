package ru.valkeru.msdp5prj6;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Third extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.third);
        super.onCreate(savedInstanceState);
    }

    public void onBackClick(View view) {
        onBackPressed();
    }
}
