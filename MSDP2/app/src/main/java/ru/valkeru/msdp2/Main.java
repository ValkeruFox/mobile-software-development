package ru.valkeru.msdp2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main extends AppCompatActivity {
    private Button btnAdd;
    private Button btnCopy;
    private TextView tView1;
    private TextView tView2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.main);
        super.onCreate(savedInstanceState);

        btnAdd = findViewById(R.id.btdAdd);
        btnCopy = findViewById(R.id.btnCopy);
        tView1 = findViewById(R.id.tView1);
        tView2 = findViewById(R.id.tView2);
    }

    public void onAddClick(View view) {
        String text = tView1.getText().toString();
        text += "*";
        tView1.setText(text);
    }

    public void onCopyClick(View view) {
        String tView1Text = tView1.getText().toString();
        tView2.setText(tView1Text);
    }
}
