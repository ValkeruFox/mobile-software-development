package ru.valkeru.msdp2pr3;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Main extends AppCompatActivity {
    private LinearLayout mainLayout;
    private TextView textView1;
    private TextView textView2;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.main);
        super.onCreate(savedInstanceState);

        mainLayout = findViewById(R.id.mainLayout);
        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);
    }

    public void changeTextViewsColorsToRedGreen(View view) {
        textView1.setBackground(getDrawable(R.drawable.red_background_border));
        textView1.setTextColor(getResources().getColor(R.color.green, null));
        textView2.setBackground(getDrawable(R.drawable.red_background_border));
        textView2.setTextColor(getResources().getColor(R.color.green, null));
    }

    public void changeTextViewsColorsToYellowBlue(View view) {
        textView1.setBackground(getDrawable(R.drawable.yellow_background_border));
        textView1.setTextColor(getResources().getColor(R.color.blue, null));
        textView2.setBackground(getDrawable(R.drawable.yellow_background_border));
        textView2.setTextColor(getResources().getColor(R.color.blue, null));
    }

    public void changeTextViewsColorsToBlackWhite(View view) {
        textView1.setBackground(getDrawable(R.drawable.black_background_border));
        textView1.setTextColor(getResources().getColor(R.color.white, null));
        textView2.setBackground(getDrawable(R.drawable.black_background_border));
        textView2.setTextColor(getResources().getColor(R.color.white, null));
    }

    public void changeBackgroundToRed(View view) {
        mainLayout.setBackgroundColor(getResources().getColor(R.color.red, null));
    }

    public void changeBackgroundToBlue(View view) {
        mainLayout.setBackgroundColor(getResources().getColor(R.color.blue, null));
    }

    public void changeBackgroundToBlack(View view) {
        mainLayout.setBackgroundColor(getResources().getColor(R.color.black, null));
    }
}
